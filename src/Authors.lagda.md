```

module Authors where
```

# About the Authors

This is a page where everyone who adds to the 1lab can write a little
bit about themselves. I mean it: everyone can write a bit about
themselves! Try to follow the format of existing profiles [in the source
file], and keep the description short. Don't forget to mention your
pronouns.

[in the source file]: https://gitlab.com/plt_amy/cubical-1lab/-/blob/main/src/Authors.lagda.md

<!-- KEEP THIS SVG HERE -->

<svg width="0" height="0">
  <clipPath id="squircle" clipPathUnits="objectBoundingBox">
    <path
      fill="red"
      stroke="none"
      d="M 0,0.5 C 0,0 0,0 0.5,0 S 1,0 1,0.5 1,1 0.5,1 0,1 0,0.5"
    />
  </clipPath>
</svg>

<!-- ALTERNATE pfp-left AND pfp-right CLASSES FOR EACH PROFILE -->

<div class="profile pfp-left">
<div class="profile-pfp">
<img alt="Amélia's profile picture" src="/static/pfps/amelia.png" />
<span class="profile-name">Amélia</span>
<span class="profile-pronouns">they/them</span>
<span><a href="https://twitter.com/plt_amy">@plt_amy</a></span>
</div>

<div class="profile-profile">
Hi! I'm Amélia, a non-binary programmer/mathematician. I'm 19, but I've
been programming since age 7, and doing dependently-typed programming
since 14. Last year, I implemented [my own cubical type theory], to
figure out what it was all about.

Since then, I've realised that my energy would be better spent making
_existing_ type theories more accessible, and this project was the
result. The 1lab grew out of a personal project formalising category
theory to better my understanding of category theory, and it's currently
undergoing a rewrite from base Agda-with-K to Cubical Agda.

[my own cubical type theory]: https://git.amelia.how/amelia/cubical

My other notable projects include [my personal blog], where I write
about type theory and the implementation of programming languages, and
[Amulet], a ML-family language with an advanced type system.

[my personal blog]: https://amelia.how
[Amulet]: https://amulet.works
</div>
</div>
